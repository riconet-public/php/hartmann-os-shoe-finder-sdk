<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\HartmannOsShoeFinderSdk;

use GuzzleHttp\Client as HttpClient;
use InvalidArgumentException;
use PSVneo\HartmannOsShoeFinderSdk\Api\Mutation;
use PSVneo\HartmannOsShoeFinderSdk\Api\MutationInterface;
use PSVneo\HartmannOsShoeFinderSdk\Api\Query;
use PSVneo\HartmannOsShoeFinderSdk\Api\QueryInterface;
use PSVneo\HartmannOsShoeFinderSdk\Configuration\ClientConfiguration;
use PSVneo\HartmannOsShoeFinderSdk\GraphQL\FieldToQueryConverter;
use RuntimeException;

class Client implements ClientInterface
{
    protected const AUTH_ROUTE = '/auth/login-user';

    protected const VERIFY_ROUTE = '/auth/verify';

    /**
     * @var string
     */
    protected $baseUrl = '';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $password = '';

    /**
     * @var HttpClient
     */
    protected $client;

    public function __construct(ClientConfiguration $configuration)
    {
        $this->baseUrl = $configuration->getBaseUrl();
        $this->email = $configuration->getEmail();
        $this->password = $configuration->getPassword();
        $this->initClient();
    }

    public function query(): QueryInterface
    {
        if (!$this->tokenIsValid()) {
            $this->connect();
        }
        if (!$this->tokenIsValid()) {
            throw new RuntimeException('Could not connect to API');
        }

        return new Query($this->client, new FieldToQueryConverter());
    }

    public function mutation(): MutationInterface
    {
        if (!$this->tokenIsValid()) {
            $this->connect();
        }
        if (!$this->tokenIsValid()) {
            throw new RuntimeException('Could not connect to API');
        }

        return new Mutation($this->client);
    }

    protected function initClient(?string $jwt = null): void
    {
        $headers = ['Accept' => 'application/json'];
        if (is_string($jwt)) {
            $headers['Authorization'] = "Bearer $jwt";
        }
        $this->client = new HttpClient([
            'base_uri' => $this->baseUrl,
            'headers' => $headers,
        ]);
    }

    protected function connect(): void
    {
        $response = $this->client->request('POST', $this->baseUrl . self::AUTH_ROUTE, [
            'json' => [
                'email' => $this->email,
                'password' => $this->password,
            ],
        ]);
        $result = json_decode((string) $response->getBody(), true);
        if (201 !== $response->getStatusCode()) {
            throw new InvalidArgumentException('Invalid client id!');
        }
        if (!isset($result['access_token']) || empty($result['access_token'])) {
            throw new RuntimeException('Could not fetch access token');
        }
        $this->initClient($result['access_token']);
    }

    protected function tokenIsValid(): bool
    {
        $response = $this->client->request('POST', $this->baseUrl . self::VERIFY_ROUTE);
        $result = json_decode((string) $response->getBody(), true);

        return isset($result['success']) && $result['success'];
    }
}
