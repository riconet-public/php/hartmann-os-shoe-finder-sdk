<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\HartmannOsShoeFinderSdk\Api;

use GuzzleHttp\ClientInterface;

interface MutationInterface
{
    public function __construct(ClientInterface $client);

    public function createArticle(array $input): array;

    public function updateArticle(array $input): array;

    public function deleteArticle(int $id): bool;

    public function createManufacturer(array $input): array;

    public function updateManufacturer(array $input): array;

    public function deleteManufacturer(int $id): bool;

    public function uploadMedia(array $input): string;

    public function removeMedia(int $mediaType, string $fileName): bool;

    public function importArticles(array $inputs): array;

    public function importManufacturers(array $inputs): array;
}
