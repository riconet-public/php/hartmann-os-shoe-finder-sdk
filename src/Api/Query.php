<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\HartmannOsShoeFinderSdk\Api;

use GuzzleHttp\ClientInterface;
use PSVneo\HartmannOsShoeFinderSdk\GraphQL\Fields;
use PSVneo\HartmannOsShoeFinderSdk\GraphQL\FieldToQueryConverterInterface;
use PSVneo\HartmannOsShoeFinderSdk\Utility\StringUtility;

class Query implements QueryInterface
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var FieldToQueryConverterInterface
     */
    protected $fieldConverter;

    public function __construct(ClientInterface $client, FieldToQueryConverterInterface $fieldConverter)
    {
        $this->client = $client;
        $this->fieldConverter = $fieldConverter;
    }

    public function custom(string $query, ?array $variables = null): array
    {
        return $this->fetch($query, $variables);
    }

    public function getArticles(int $page, int $count, string $language): array
    {
        $fields = $this->fieldConverter->convert(Fields::ARTICLE);
        $query = "{articles(page: $page, first: $count, language: $language)$fields}";

        return $this->fetch($query);
    }

    public function getManufacturers(int $page, int $count, string $language): array
    {
        $fields = $this->fieldConverter->convert(Fields::MANUFACTURER);
        $query = "{manufacturers(page: $page, first: $count, language: $language)$fields}";

        return $this->fetch($query);
    }

    public function getMedias(string $mediaType): array
    {
        $mediaType = Fields::MEDIA_TYPES[$mediaType];
        $fields = $this->fieldConverter->convert(Fields::MEDIA);
        $query = "{medias(mediaType: $mediaType)$fields}";

        return $this->fetch($query);
    }

    public function articlesFilterFrontend(int $page, int $count, array $input, string $language): array
    {
        $fields = $this->fieldConverter->convert(Fields::FRONTEND_FILTER_RESULT);
        $query = StringUtility::trimGraphQLQueryString('
            query ArticlesFilterFrontend(
                $page: Int!
                $first: Int!
                $input: FrontendFilterInput
                $language: Language!
            ) {
                articlesFilterFrontend(page: $page, first: $first, input: $input, language: $language) ' . $fields . '
            }
        ');

        return $this->fetch($query, [
            'page' => $page,
            'first' => $count,
            'input' => $input,
            'language' => $language,
        ]);
    }

    public function articlesSearchFrontend(int $page, int $count, string $searchPhrase, string $language): array
    {
        $fields = $this->fieldConverter->convert(Fields::FRONTEND_SEARCH_RESULT);
        $query = StringUtility::trimGraphQLQueryString('
            query ArticlesSearchFrontend(
                $page: Int!
                $first: Int!
                $searchPhrase: String
                $language: Language!
            ) {
                articlesSearchFrontend(
                page: $page,
                first: $first,
                searchPhrase: $searchPhrase,
                language: $language
            ) ' . $fields . '
            }
        ');

        return $this->fetch($query, [
            'page' => $page,
            'first' => $count,
            'searchPhrase' => $searchPhrase,
            'language' => $language,
        ]);
    }

    public function articlesExportFrontend(array $input, string $language): array
    {
        $fields = $this->fieldConverter->convert(Fields::ARTICLE_EXPORT_FILE);
        $query = StringUtility::trimGraphQLQueryString('
            query ArticlesExportFrontend(
                $input: FrontendFilterInput
                $language: Language!
            ) {
                articlesExportFrontend(input: $input, language: $language) ' . $fields . '
            }
        ');

        return $this->fetch($query, [
            'input' => $input,
            'language' => $language,
        ]);
    }

    protected function fetch(string $query, ?array $variables = null): array
    {
        $response = $this->client->request('POST', '/graphql', [
            'json' => [
                'query' => $query,
                'variables' => $variables,
            ],
        ]);
        $data = json_decode((string) $response->getBody(), true);
        if (!is_array($data)) {
            return [
                'errors' => [
                    'message' => 'An error occurred while trying to decode the server response',
                ],
            ];
        }

        return $data;
    }
}
