<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\HartmannOsShoeFinderSdk\Api;

use Exception;
use GuzzleHttp\ClientInterface;

class Mutation implements MutationInterface
{
    /**
     * @var ClientInterface
     */
    protected $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function createArticle(array $input): array
    {
        throw new Exception('Not implemented');
    }

    public function updateArticle(array $input): array
    {
        throw new Exception('Not implemented');
    }

    public function deleteArticle(int $id): bool
    {
        throw new Exception('Not implemented');
    }

    public function createManufacturer(array $input): array
    {
        throw new Exception('Not implemented');
    }

    public function updateManufacturer(array $input): array
    {
        throw new Exception('Not implemented');
    }

    public function deleteManufacturer(int $id): bool
    {
        throw new Exception('Not implemented');
    }

    public function uploadMedia(array $input): string
    {
        throw new Exception('Not implemented');
    }

    public function removeMedia(int $mediaType, string $fileName): bool
    {
        throw new Exception('Not implemented');
    }

    public function importArticles(array $inputs): array
    {
        throw new Exception('Not implemented');
    }

    public function importManufacturers(array $inputs): array
    {
        throw new Exception('Not implemented');
    }
}
