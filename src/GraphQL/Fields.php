<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PSVneo\HartmannOsShoeFinderSdk\GraphQL;

class Fields
{
    public const ARTICLE = [
        'id',
        'externalId',
        'active',
        'title',
        'description',
        'ean',
        'articleNumber',
        'series',
        'protectionClass',
        'protectiveCap',
        'punctureProtection',
        'properties',
        'sizeFrom',
        'sizeTo',
        'width',
        'orthopedicallyApproved',
        'productionInstructionLink' => self::MEDIA,
        'modificationInstructionLink' => self::MEDIA,
        'optionalFileLink' => self::MEDIA,
        'conformityInternalLink' => self::MEDIA,
        'conformityExternalLink',
        'certificateEmail',
        'certificateExpirationDate',
        'manufacturer' => self::MANUFACTURER,
        'image' => self::MEDIA,
    ];

    public const MANUFACTURER = [
        'id',
        'title',
        'logo' => self::MEDIA,
        'website',
    ];

    public const MEDIA = [
        'baseName',
        'fileName',
        'mimeType',
        'publicPath',
        'absolutePath',
        'relativePath',
        'extension',
        'size',
    ];

    public const MEDIA_TYPES = [
        'Logo' => 'Logo',
        'ProductionInstruction' => 'ProductionInstruction',
        'ModificationInstruction' => 'ModificationInstruction',
        'File' => 'File',
        'Conformity' => 'Conformity',
    ];

    public const FRONTEND_FILTER_ITEMS = [
        'manufacturers' => self::MANUFACTURER,
        'series',
        'protectionClasses',
    ];

    public const FRONTEND_FILTER_RESULT = [
        'maxResults',
        'articles' => self::ARTICLE,
        'filterItems' => self::FRONTEND_FILTER_ITEMS,
    ];

    public const FRONTEND_SEARCH_RESULT = [
        'maxResults',
        'articles' => self::ARTICLE,
    ];

    public const ARTICLE_EXPORT_FILE = [
        'name',
        'mimeType',
        'content',
    ];
}
