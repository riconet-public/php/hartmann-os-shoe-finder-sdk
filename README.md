# hartmann-os-shoe-finder-sdk
This library provides an easy API to communicate with the hartmann-os-shoe-finder-api.

## Requirements
* PHP 7.2+
* composer
* ext-json

## How to install
Run `composer require psvneo/hartmann-os-shoe-finder-sdk`.

## How to use

### Instantiate a new client
```php
<?php
require_once '.../vendor/autoload.php';

$configuration = new \PSVneo\HartmannOsShoeFinderSdk\Configuration\ClientConfiguration(
    "https://api-base-url.com",
    "api-user@email.address",
    "secret"
);
$client = new \PSVneo\HartmannOsShoeFinderSdk\Client($configuration);

```
### Fetch data (QUERY)
```php
<?php

# Fetch list of articles.
$data = $client->query()->getArticles(
    1,  // page
    10, // record count
    'DE' // Language code
);

# Fetch list of manufacturers.
$data = $client->query()->getManufacturers(
    1,  // page
    10, // record count
    'DE' // Language code
);

# Fetch medias of defined type.
$data = $client->query()->getMedias(
    \PSVneo\HartmannOsShoeFinderSdk\GraphQL\Fields::MEDIA_TYPES['Logo']
);

# Run custom query against the server with variables.
$data = $client->query()->custom(
    \PSVneo\HartmannOsShoeFinderSdk\Utility\StringUtility::trimGraphQLQueryString('
        query Articles ($language: Language!) {
            articles(language: $language) {
                id
                title
            }
        }
    '),
    [
        "language" => 'EN'
    ]
);
```

### Manipulate data (MUTATION)
Yet not implemented!
